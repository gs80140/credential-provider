﻿using CredProvider.NET.Interop2;
using System.Runtime.InteropServices;
using static CredProvider.NET.Constants;

namespace CredProvider.NET
{
    /// <summary>
    /// https://learn.microsoft.com/en-us/windows/win32/api/credentialprovider/ns-credentialprovider-credential_provider_field_descriptor
    /// </summary>
    public abstract class CredentialProviderBase : ICredentialProvider, ICredentialProviderSetUserArray
    {
        private ICredentialProviderEvents? events;

        protected abstract CredentialView Initialize(_CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus, uint dwFlags);

        private CredentialView? view;
        private _CREDENTIAL_PROVIDER_USAGE_SCENARIO usage;

        private readonly List<ICredentialProviderUser> providerUsers = new();


        /// <summary>
        /// 调用初始化登录选项
        /// </summary>
        /// <param name="cpus"></param>
        /// <param name="dwFlags"></param>
        /// <returns></returns>
        public virtual int SetUsageScenario(_CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus, uint dwFlags)
        {
            view = Initialize(cpus, dwFlags);
            usage = cpus;

            if (view.Active)
            {
                return HRESULT.S_OK;
            }

            return HRESULT.E_NOTIMPL;
        }


        /// <summary>
        /// 用于将凭据序列化为一个字符串
        /// </summary>
        /// <param name="pcpcs"></param>
        /// <returns></returns>
        public virtual int SetSerialization(ref _CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION pcpcs)
        {
            Logger.Write($"ulAuthenticationPackage: {pcpcs.ulAuthenticationPackage}");

            return HRESULT.S_OK;
        }


        /// <summary>
        /// 用于向凭据提供程序发送事件通知
        /// </summary>
        /// <param name="pcpe"></param>
        /// <param name="upAdviseContext"></param>
        /// <returns></returns>
        public virtual int Advise(ICredentialProviderEvents pcpe, ulong upAdviseContext)
        {
            Logger.Write($"upAdviseContext: {upAdviseContext}");

            if (pcpe != null)
            {
                events = pcpe;

                Marshal.AddRef(Marshal.GetIUnknownForObject(pcpe));
            }

            return HRESULT.S_OK;
        }

        /// <summary>
        /// 取消通知
        /// </summary>
        /// <returns></returns>
        public virtual int UnAdvise()
        {
            Logger.Write();

            if (events != null)
            {
                //Marshal.Release(Marshal.GetIUnknownForObject(events));
                events = null;
            }

            return HRESULT.S_OK;
        }

        public virtual int GetFieldDescriptorCount(out uint pdwCount)
        {
            Logger.Write();

            pdwCount = (uint)(view?.DescriptorCount ?? 0);

            Logger.Write($"Returning field count: {pdwCount}");

            return HRESULT.S_OK;
        }


        /// <summary>
        /// 用于获取凭据提供程序中的凭据字段数
        /// </summary>
        /// <param name="dwIndex"></param>
        /// <param name="ppcpfd"></param>
        /// <returns></returns>
        public virtual int GetFieldDescriptorAt(uint dwIndex, [Out] IntPtr ppcpfd)
        {
            if (view?.GetField((int)dwIndex, ppcpfd) ?? false)
            {
                return HRESULT.S_OK;
            }

            return HRESULT.E_INVALIDARG;
        }


        /// <summary>
        /// 用于获取凭据提供程序中的凭据数
        /// </summary>
        /// <param name="pdwCount"></param>
        /// <param name="pdwDefault"></param>
        /// <param name="pbAutoLogonWithDefault"></param>
        /// <returns></returns>
        public virtual int GetCredentialCount(
            out uint pdwCount,
            out uint pdwDefault,
            out int pbAutoLogonWithDefault
        )
        {
            Logger.Write();

            pdwCount = (uint)(view?.CredentialCount ?? 0);

            pdwDefault = (uint)(view?.DefaultCredential ?? 0);

            pbAutoLogonWithDefault = 0;

            return HRESULT.S_OK;
        }


        /// <summary>
        /// 用于获取指定索引处的凭据
        /// </summary>
        /// <param name="dwIndex"></param>
        /// <param name="ppcpc"></param>
        /// <returns></returns>
        public virtual int GetCredentialAt(uint dwIndex, out ICredentialProviderCredential? ppcpc)
        {
            Logger.Write($"dwIndex: {dwIndex}");

            ppcpc = view?.CreateCredential((int)dwIndex);

            return HRESULT.S_OK;
        }


        /// <summary>
        /// 用于获取凭据字段的用途
        /// </summary>
        /// <returns></returns>
        public virtual _CREDENTIAL_PROVIDER_USAGE_SCENARIO GetUsage()
        {
            return usage;
        }

        /// <summary>
        /// 用于向凭据提供程序提供有关当前登录会话中的用户的信息
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public virtual int SetUserArray(ICredentialProviderUserArray users)
        {
            this.providerUsers.Clear();

            users.GetCount(out uint count);
            users.GetAccountOptions(out CREDENTIAL_PROVIDER_ACCOUNT_OPTIONS options);

            Logger.Write($"count: {count}; options: {options}");

            for (uint i = 0; i < count; i++)
            {
                users.GetAt(i, out ICredentialProviderUser user);

                user.GetProviderID(out Guid providerId);//返回凭据提供程序的GUID。这个GUID是用于标识凭据提供程序的唯一标识符。
                user.GetSid(out string sid);//用于获取指定用户或组的安全标识符（SID）。SID是一个唯一的标识符，用于标识用户或组在Windows系统中的安全上下文中的身份。GetSid函数

                this.providerUsers.Add(user);

                Logger.Write($"providerId: {providerId}; sid: {sid}");
            }

            Logger.Write($"Set providerUsers to array with length {this.providerUsers.Count}");

            return HRESULT.S_OK;
        }

        /// <summary>
        /// 用于获取当前登录用户的安全标识符（SID）。SID是一个唯一的标识符，用于标识用户在Windows系统中的安全上下文中的身份
        /// </summary>
        /// <param name="dwIndex"></param>
        /// <returns></returns>
        public virtual string GetUserSid(int dwIndex)
        {
            Logger.Write($"Total users: {providerUsers.Count}");
            string sid = string.Empty;

            //CredUI does not provide user sids, so return null
            if (this.providerUsers.Count < dwIndex + 1)
            {
                Logger.Write($"Not enough users in the array, total {providerUsers.Count} users, asking for #{dwIndex}");
                return sid;
            }

            try
            {
                this.providerUsers[dwIndex].GetSid(out sid);
                Logger.Write($"returning user {sid}");
            }
            catch (Exception e)
            {
                Logger.Write($"Exception getting user {dwIndex}: {e.Message}");
            }
            
            return sid;
        }
    }
}
