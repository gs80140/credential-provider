﻿using CredProvider.NET.Interop2;
using System.Runtime.InteropServices;

namespace CredProvider.NET
{
    public class CredentialDescriptor
    {
        public _CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR Descriptor { get; set; }

        public _CREDENTIAL_PROVIDER_FIELD_STATE State { get; set; }

        public object Value { get; set; } = new();
    }

    public class CredentialView
    {
        private readonly List<CredentialDescriptor> fields = new();

        public CredentialProviderBase Provider { get; private set; }

        public const string CPFG_LOGON_USERNAME = "da15bbe8-954sd-4fd3-b0f4-1fb5b90b174b";//用于输入登录用户名的凭据字段。
        public const string CPFG_LOGON_PASSWORD = "60624cfa-a477-47b1-8a8e-3a4a19981827";//用于输入登录密码的凭据字段。
        public const string CPFG_SMARTCARD_USERNAME = "3e1ecf69-568c-4d96-9d59-46444174e2d6";//用于标识用于输入智能卡用户名的凭据字段。
        public const string CPFG_SMARTCARD_PIN = "4fe5263b-9181-46c1-b0a4-9dedd4db7dea";//用于标识用于输入智能卡 PIN 的凭据字段
        public const string CPFG_CREDENTIAL_PROVIDER_LOGO = "2d837775-f6cd-464e-a745-482fd0b47493";//用于显示凭据提供程序的标志。
        public const string CPFG_CREDENTIAL_PROVIDER_LABEL = "286bbff3-bad4-438f-b007-79b7267c3d48";//用于显示凭据提供程序的名称。

        public bool Active { get; set; }

        public int DescriptorCount { get { return fields.Count; } }

        public virtual int CredentialCount { get { return 1; } }

        public virtual int DefaultCredential { get { return 0; } }        

        public CredentialView(CredentialProviderBase provider) 
        {
            Provider = provider;
        }

        /// <summary>
        /// 用于向凭据提供程序添加一个新的凭据字段
        /// </summary>
        /// <param name="cpft">用于指定凭据字段的类型</param>
        /// <param name="pszLabel">字段名称</param>
        /// <param name="state">用于指定凭据字段的状态</param>
        /// <param name="defaultValue">字段默认值</param>
        /// <param name="guidFieldType">GUID 类型的变量，用于标识凭据字段的类型</param>
        /// <exception cref="NotSupportedException"></exception>
        public virtual void AddField(_CREDENTIAL_PROVIDER_FIELD_TYPE cpft, string pszLabel, _CREDENTIAL_PROVIDER_FIELD_STATE state, string? defaultValue = null, Guid guidFieldType = default)
        {
            if (!Active)
            {
                throw new NotSupportedException();
            }

            fields.Add(new CredentialDescriptor
            {
                State = state,
                Value = defaultValue ?? string.Empty,
                Descriptor = new _CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR
                {
                    dwFieldID = (uint)fields.Count,
                    cpft = cpft,
                    pszLabel = pszLabel,
                    guidFieldType = guidFieldType
                }
            });
        }

        public virtual bool GetField(int dwIndex, [Out] IntPtr ppcpfd)
        {
            Logger.Write($"dwIndex: {dwIndex}; descriptors: {fields.Count}");

            if (dwIndex >= fields.Count)
            {
                return false;
            }

            var field = fields[dwIndex];

            var pcpfd = Marshal.AllocHGlobal(Marshal.SizeOf(field.Descriptor));

            Marshal.StructureToPtr(field.Descriptor, pcpfd, false);
            Marshal.StructureToPtr(pcpfd, ppcpfd, false);

            return true;
        }

        public string GetValue(int dwFieldId)
        {
            return (string)fields[dwFieldId].Value;
        }

        public void SetValue(int dwFieldId, string val)
        {
            fields[dwFieldId].Value = val;
        }

        public void GetFieldState(
            int dwFieldId,
            out _CREDENTIAL_PROVIDER_FIELD_STATE pcpfs,
            out _CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE pcpfis
        )
        {
            Logger.Write();

            var field = fields[dwFieldId];

            Logger.Write($"Returning field state: {field.State}, interactiveState: None");

            pcpfs = field.State;
            pcpfis = _CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE.CPFIS_NONE;
        }

        private readonly Dictionary<int, ICredentialProviderCredential> credentials
            = new Dictionary<int, ICredentialProviderCredential>();

        public virtual ICredentialProviderCredential CreateCredential(int dwIndex)
        {
            Logger.Write();

            if (credentials.TryGetValue(dwIndex, out ICredentialProviderCredential? credential))
            {
                Logger.Write("返回现有凭据");
                return credential;
            }

            //Get the sid for this credential from the index
            var sid = this.Provider.GetUserSid(dwIndex);
            Logger.Write($"获取新的sid: {sid}");

            try
            {
                credential = new CredentialProviderCredential(this, sid);
                Logger.Write($"新凭据创建成功");
            }
            catch (Exception e)
            {
                Logger.Write($"Exception thrown: {e.Message}");
                throw;
            }
            

            credentials[dwIndex] = credential;

            Logger.Write("返回新凭据");
            return credential;
        }
    }
}
