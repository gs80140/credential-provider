﻿using CredProvider.NET.Interop2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CredProvider.NET
{
    static class Common
    {
        /// <summary>
        /// 它用于检索用于进行身份验证的 Negotiate 身份验证包的标识符。
        /// Negotiate 身份验证包是一种 Windows 认证提供程序，它提供了用于验证用户凭据的功能。
        /// 当用户尝试登录到 Windows 计算机时，Windows 安全子系统会使用 Negotiate 身份验证包来验证用户的用户名和密码。
        /// 如果用户名和密码是有效的，则用户将被授予访问计算机的权限。
        /// 如果用户名和密码无效，则用户将被拒绝访问计算机。
        ///RetrieveNegotiateAuthPackage 函数的参数是一个指向 PULONG 类型的指针，该指针用于接收 Negotiate 身份验证包的标识符。
        /// 如果函数成功，则返回 STATUS_SUCCESS。否则，函数将返回一个错误代码，您可以使用 NTSTATUS 宏来解释该代码。
        /// 确定所需的身份验证包
        /// </summary>
        /// <param name="authPackage"></param>
        /// <returns></returns>
        public static int RetrieveNegotiateAuthPackage(out uint authPackage)
        {
            Logger.Write();

            var status = PInvoke.LsaConnectUntrusted(out var lsaHandle);

            //使用协商允许 LSA 决定是使用本地身份验证包还是 Kerberos 身份验证包
            //Yubikey sub auth module link: https://github.com/Yubico/yubico-windows-auth
            using (var name = new PInvoke.LsaStringWrapper("Negotiate"))
            {
                status = PInvoke.LsaLookupAuthenticationPackage(lsaHandle, ref name._string, out authPackage);
            }

            PInvoke.LsaDeregisterLogonProcess(lsaHandle);

            Logger.Write($"使用身份验证包 ID: {authPackage}");

            return (int)status;
        }

        public static string GetNameFromSid(string value)
        {
            Logger.Write();

            var sid = new SecurityIdentifier(value);
            var ntAccount = (NTAccount)sid.Translate(typeof(NTAccount));
            var name= ntAccount.ToString();
            Logger.Write("账户名称:"+name);
            return name;
        }
    }
}
